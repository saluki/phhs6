---
title: "Inventory Turnover"
tags:
- business
---

**Inventory turnover measures how often each year a business sells and replaces its inventory.**
*Bigger is better.*


### Main types of inventory

- Raw Materials & Components
- Work in progress
- Finished goods

### Calculating inventory turnover

Inventory Turnover = Cost of Sales / Inventories

So if cost of sales was £500,000 and inventories was £50,000 then inventory turnover would be 10 (times per year).

Cost of sales from income statement
Inventories from balance sheet

### Receivables 


Receivables (days) = Trade receivables / revenue (sales) x 365

### Payables

Payables (days) = Trade payables / cost of sales x 365

### Evaluating inventory turnover

- Varies between industries
- Holding more inventory may improve customer service
- Seasonal fluctuations in demand during the year may not be reflected in the calculations
- Inventory turnover is not relevant to most service businesses

### How can inventory turnover be increased?

- Sell-off or dispose of slow-moving or obsolete inventory
- Introduce lean production techniques to reduce amounts of inventory held
- Rationalise the product range made or sold
- Negotiate “sale or return” arrangements with suppliers – so inventory can be returned if it does not sell

### Limitations of Ratio Analysis

- One data set is not enough
- Unknown reliability of data
	- Window dressing
- Based on the past
- Comparability

### Why might ratio data not be entirely reliable?

- Financial information involves making subjective judgements
- Different businesses have different accounting policies
- Potential for manipulation of accounting information (window dressing)

### What ratios don't show

- Competitive advantages (brand strength etc)
- Quality
- Ethical reputation
- Future prospects
- Changes in the external environment


### Ratios Summary

- Very useful analytical tools
- Widely used and understood
- Identify issues but don't solve problems
- Part of a range of indicators of business problems

---
*Following is work not notes*

### Practice Exercise 11

- Which ratio would be best for:
1) how effective inventory control is -> Inventory Turnover
2) whether shareholders are likely to be happy with their share of the profit: ROCE
3) whether a business is likely to be able to avoid a liquidity problem in the short term if it can convert all of its liquid assets into cash: Current Ratio
4) how effective a business is in turning its raw materials or finished products into products with higher value: Gross Profit Margin
5) whether a business is likely to experience a liquidity problem in the long term: Gearing
6) how successful the business is in using its capital to generate profit: ROCE
7) how quickly a business is receiving money from customers who buy its good on credit: Receivables days
8) whether a business is successful in generating profit from its usual business activities, in comparison to the sales revenue that it receives: Operating Profit Margin
9) whether suppliers are providing the business with good credit terms: Gearing
10) how vulnerable the business might be if there is a dramatic increase in interest rates: Gearing


### Practice Exercise 12

*Planned responses only.*

1) Explain two factors that may cause the information used in a firm's ratio analysis to be unreliable.
   
   Window dressing is capable of masking the true financial situation of the business, so if used correctly a firm may be able to appear as though it is in a better situation than it actually is.
   
   Financial reports are also historical, which means they only reflect what the situation of the business *was*, not what it is currently. This makes them ineffective at identifying any short term trends or changes in the business.

2) State two types of comparison used in ratio analysis
   
   - Direct comparisons of ratios – works within an industry
   - Historial comparisons - look at trends between companies



[Business](/Business)