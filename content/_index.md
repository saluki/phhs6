---
title: "Home"
description: "Landing page"
---

This site hosts various work I do openly, with the intention of creating an easily accessible, free knowledgebase. Although please do keep in mind that this site is not intended to be used as an authoritative information source.

My biology work is not done here primarily, so it is copied up after being done in batches. This may result in significant delays.

Content on this site may not all be owned or produced by me, and is hosted here under fair use. If you have the rights to any content here and wish to have it altered or removed, then please contact me to request this.

The accuracy of the information on the site cannot be fully guaranteed. I do my best but as I am learning these subjects some inaccuracies are inevitable. If you notice something, please let me know or submit a correction on GitHub. I will appreciate any feedback.



# Subjects

I have attached a link to all work for each subject below.

### 💻[Computer Science](/tags/compsci)

### 🦠[Biology](/tags/biology)

### 💰[Business Studies](/tags/business)


Other, miscellaneous work may be found via the search function or the interactive graph. 


If you notice an issue with the site or need help reach out to me on [Mastodon](https://fosstodon.org/@saluki)


To support my work here and elsewhere, and encourage me to continue to work in the open, making a donation with the button below is the best way to help out. Any amount is appreciated.

{{< htmlsnippet "layouts/partials/donate.html" >}}

> [!tip] Come and have a chat 
>
> I also host a [community forum](https://community.sethmb.xyz/public)

## FAQ

### Does this website cover the whole A level spec for each subject?

No. Nor is it intended to. Chances are that by the time I have finished studying the course, it will cover *most* of the spec *for a specific exam board* per *subject*.

### Can I freely copy content from this website?

As the rights to content hosted here are not as straightforward as me being able to freely give everything away, I would strongly recommend you contact me before redistributing or modifying content.

### Can I use this to revise?

Yes, go for it. However, if something doesn't look right, challenge it. I would also strongly recommend combining this with other revision techniques, such as flash cards (see [Anki](https://ankiweb.net)).

### How easy is to make my own website like this?

Fairly straightforward. I recommend having a look at [Quartz](https://github.com/jackyzha0/quartz) for more details, this is the same tool I am using here. You probably want (but don't need) a custom domain, and you likely want [Obsidian](https://obsidian.md) . If you would like some help with setup, feel free to reach out to me and I will give you a hand.