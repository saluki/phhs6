---
title: "Privacy Policy"
---

> [!info] Disclaimer
>
> This website is built as a hobby project - it is intended mainly for personal use and is provided publicly free of charge. I go out of my way to try and reduce data gathered, keeping things fairly minimal, but due to the use of third-party tools I cannot guarantee this.

**What data we collect**

*Matamo* is used to collect basic analytical data on site visitors—this data is used solely for optimising the website. No personally identifiable data is collected. This is set to respect Do Not Track requests from your browser, so enabling Do Not Track will automatically opt you out of data collection. You are also welcome to block scripts from *me.sethmb.xyz/analytics* from being loaded should you wish to ensure no communication occurs.


We host the site on **GitHub Pages**. This means some information about you and your browser will be provided to GitHub—their privacy policy can be found here: [GitHub privacy](https://docs.github.com/en/site-policy/privacy-policies/github-privacy-statement)

DNS records are created and managed via [Cloudflare](https://cloudflare.com) however this website does not directly utilise any services other than the core authoratitive DNS service. 


[Go home](/)